FROM node:13.12.0 as builder

WORKDIR /app

COPY package.json      .
COPY package-lock.json .

RUN npm install

COPY . .

RUN npm run build --prod

FROM nginx:1.16.0-alpine

COPY --from=builder /app/dist/frontend /usr/share/nginx/html
