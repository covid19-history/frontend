import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Event as CoronaEvent } from '../../models/event';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';

const addReactions = gql`
    mutation addReactions($id: ID!, $data: UpdateReactionsData) {
        updateEventReactions(id: $id, data: $data) {
            frowns
        }
    }
`;

@Component({
    selector: 'app-event-thumb',
    templateUrl: './event-thumb.component.html',
    styleUrls: ['./event-thumb.component.scss'],
})
export class EventThumbComponent implements OnInit {
    @Input() event: CoronaEvent;

    constructor(private apollo: Apollo) {}

    ngOnInit(): void {}

    public async addReaction(id: string, reaction: string) {
        this.apollo
            .mutate({
                mutation: addReactions,
                variables: {
                    id,
                    data: {
                        [reaction]: 1,
                    },
                },
            })
            .subscribe()
            .unsubscribe();
        this.event.reactions[reaction] += 1;
    }
}
