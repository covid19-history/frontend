import { Injectable } from '@angular/core';
import { Query } from 'apollo-angular';
import gql from 'graphql-tag';
import { Event as CoronaEvent } from '../models/event';

export interface Response {
    events: CoronaEvent[];
}

@Injectable({
    providedIn: 'root',
})
export class EventService extends Query<Response> {
    document = gql`
        query allEvents {
            events {
                id
                title
                summary
                description
                time
                sources {
                    name
                    link
                }
                category
                reactions {
                    claps
                    hearts
                    thumbUps
                    thumbDowns
                    smiles
                    frowns
                }
            }
        }
    `;
}
