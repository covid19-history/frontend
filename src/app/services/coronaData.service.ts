import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Day } from '../models/day';
import { tap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class CoronaDataService {
    private apiURL =
        'https://wuhan-coronavirus-api.laeyoung.endpoint.ainize.ai/jhu-edu/timeseries?iso2=CH&onlyCountries=true';

    constructor(private httpClient: HttpClient) {}

    public getDays() {
        return this.httpClient.get(this.apiURL);
    }
}
