import { Component, OnInit } from '@angular/core';
import { CoronaDataService } from '../../services/coronaData.service';
import { Day } from '../../models/day';
import { EventService } from '../../services/event.service';
import { Event as CoronaEvent } from '../../models/event';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

// @ts-ignore
Date.prototype.addDays = function (days) {
    const dat = new Date(this.valueOf());
    dat.setDate(dat.getDate() + days);
    return dat;
};

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
    public currentPage = 0;
    public lastPage: number;
    public currentDay = new Date();
    public days: Array<Day> = [];
    public coronaEvents: Observable<CoronaEvent[]>;
    private dates: Array<Date> = [];

    constructor(
        private coronaDataService: CoronaDataService,
        private eventService: EventService,
        private router: Router
    ) {}

    async ngOnInit() {
        this.setDates(new Date(2019, 11, 1, 0, 0, 0, 0), new Date());
        this.setPageConstraints();
        this.setCoronaData();
        this.setEvents();
    }

    public showCoronaEvent(coronaEvent: CoronaEvent): boolean {
        return (
            this.getFormattedDate(new Date(coronaEvent.time)) ===
            this.getFormattedDate(this.currentDay)
        );
    }

    public getFormattedDate(date: Date) {
        const day = date.getDate();
        let month = date.getMonth();
        month = month + 1;
        let newDay = String(day);
        let newMonth = String(month);
        if (newDay.length === 1) {
            newDay = '0' + day;
        }
        if (newMonth.length === 1) {
            newMonth = '0' + month;
        }
        return newDay + '.' + newMonth + '.' + date.getFullYear();
    }

    getDates() {
        return this.dates.slice(this.currentPage * 7, this.currentPage * 7 + 7);
    }

    setActiveDay(date: Date): void {
        this.currentDay = date;
    }

    nextDay(): void {
      this.currentDay.setDate(this.currentDay.getDate() + 1);
    }

    previousDay(): void {
      this.currentDay.setDate(this.currentDay.getDate() - 1);
    }

    nextPage(): void {
        this.currentPage !== this.lastPage && this.currentPage++;
    }

    previousPage(): void {
        this.currentPage !== 0 && this.currentPage--;
    }

    getDay() {
        const day = this.days.find((dayToFind) => {
            return (
                this.getFormattedDate(this.currentDay) ===
                this.getFormattedDate(dayToFind.date)
            );
        });
        if (day) {
            return day;
        }
        return {
            date: 'nan',
            infections: '0',
            deaths: '0',
            recovered: '0',
        };
    }

    private setEvents() {
        this.coronaEvents = this.eventService.watch().valueChanges.pipe(
            map((result) => {
                return result.data.events;
            })
        );
    }

    private setDates(startDate, stopDate) {
        let currentDate = startDate;
        while (currentDate <= stopDate) {
            this.dates.push(currentDate);
            currentDate = currentDate.addDays(1);
        }
    }

    private setPageConstraints() {
        this.lastPage = Math.ceil(this.dates.length / 7);
    }

    private setCoronaData() {
        this.coronaDataService.getDays().subscribe((data: any) => {
            for (const [key, value] of Object.entries(data[0].timeseries)) {
                const date = key.split('/');
                const push = {
                    date: new Date(
                        2000 + Number(date[2]),
                        Number(date[0]) - 1,
                        Number(date[1])
                    ),
                    // @ts-ignore
                    infections: value.confirmed,
                    // @ts-ignore
                    deaths: value.deaths,
                    // @ts-ignore
                    recovered: value.recovered,
                };
                this.days.push(push);
            }
        });
    }
}
