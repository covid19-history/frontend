import { Source } from './source';
import {Reactions} from './reactions';

export interface Event {
    id: string;
    title: string;
    summary: string;
    description: string;
    sources: Source[];
    time: string;
    category: string;
    reactions: Reactions;
}
