export interface Reactions {
  claps: number;
  hearts: number;
  thumbUps: number;
  thumbDowns: number;
  smiles: number;
  frowns: number;
}
