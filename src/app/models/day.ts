export interface Day {
    date: Date;
    infections: string;
    deaths: string;
    recovered: string;
}
