import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {
    MDBBootstrapModulesPro,
    MDBSpinningPreloader,
} from 'ng-uikit-pro-standard';
import { HomeComponent } from './views/home/home.component';
import { PageNotFoundComponent } from './views/page-not-found/page-not-found.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavigationComponent } from './layout/navigation/navigation.component';
import { HttpClientModule } from '@angular/common/http';
import { EventThumbComponent } from './components/event-thumb/event-thumb.component';
import { GraphQLModule } from './graphql.module';
import { ImpressumComponent } from './views/impressum/impressum.component';
import { InfosComponent } from './views/infos/infos.component';
import { CreditsComponent } from './views/credits/credits.component';

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        PageNotFoundComponent,
        NavigationComponent,
        EventThumbComponent,
        ImpressumComponent,
        InfosComponent,
        CreditsComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        MDBBootstrapModulesPro.forRoot(),
        ReactiveFormsModule,
        GraphQLModule,
    ],
    providers: [MDBSpinningPreloader],
    bootstrap: [AppComponent],
})
export class AppModule {}
